#include <iostream>

using namespace std;

int main()
{
        //Calculate precision of single
	float x,  a=1.0; //x is the variable which will be set to 1 and a is the small number which will be added to 1
	int i=0;
	while( x !=  1.0) //While x is not equal to 1, keep adding small numbers
	{
	 a=a/2; // In the first iteration, a is 2^(-1)
	 x=1.0; // Reinitialise x as 1 to prevent the loop adding every small number to x
	 x=x+a;
	 i=i+1;
	}
	a=a*2; // At the end of the while loop, the small number is twice as large as the smallest number that can be meaningfully added to 1
	i=i-1;
	cout << "The precision of a single is: "<< a << " or 2 to the power of minus "<< i <<  endl;
	
	//Calculate precision of double
	double  y,  b=1.0;
	int j=0;
	while( y !=  1.0)
	{
	 b=b/2;
	 y=1.0;
	 y=y+b;
	 j=j+1;
	}
	b=b*2;
        j=j-1;
	cout << "The precision of a double is: "<< b << " or 2 to the power of minus " << j << endl;

	//Calculate precision of extended (C++ doesn't use quadruple)
	long double  z,  c=1.0;
	int k=0;
	while( z !=  1.0)
	{
	 c=c/2;
	 z=1.0;
	 z=z+c;
	 k=k+1;
	}
	c=c*2;
        k=k-1;
	cout << "The precision of an extended is: "<< c << " or 2 to the power of minus " << k << endl;

	
return 0;
}
